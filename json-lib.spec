Name:           json-lib
Version:        2.4
Release:        23
Summary:        JSON library for Java
License:        ASL 2.0
URL:            http://json-lib.sourceforge.net/
# https://github.com/aalmiray/Json-lib/commit/a45916508dcfe4fe048e2eac10573a4a20ed6a9a
Source0:        %{name}-%{version}.tar.xz
# https://github.com/kordamp/json-lib/commit/78cc0083692ad2e30c7bcf3929c697d605a90317
Source1:        jenkins-%{name}-%{version}.tar.xz
Source2:        http://repo.jenkins-ci.org/releases/org/kohsuke/stapler/json-lib/%{version}-jenkins-3/json-lib-%{version}-jenkins-3.pom

Patch1:  0001-fix-Handle-unbalanced-comment-string.patch

BuildRequires:  java-devel maven-local maven-shared maven-surefire-provider-junit
BuildRequires:  mvn(commons-beanutils:commons-beanutils) mvn(commons-lang:commons-lang)
BuildRequires:  mvn(commons-collections:commons-collections) mvn(junit:junit) mvn(log4j:log4j)
BuildRequires:  mvn(commons-logging:commons-logging) mvn(net.sf.ezmorph:ezmorph)
BuildRequires:  mvn(org.apache.maven.plugins:maven-antrun-plugin) mvn(oro:oro) mvn(xom:xom)
BuildRequires:  mvn(org.apache.maven.plugins:maven-site-plugin) mvn(xmlunit:xmlunit)
BuildRequires:  mvn(org.codehaus.groovy:groovy) mvn(org.codehaus.groovy:groovy-all)
BuildRequires:  mvn(org.codehaus.mojo:build-helper-maven-plugin) mvn(antlr:antlr) mvn(asm:asm-all)
BuildRequires:  mvn(commons-cli:commons-cli) mvn(org.slf4j:slf4j-nop)

BuildArch:      noarch

%description
JSON-lib is a java library for transforming beans, maps, collections, java
arrays and XML to JSON and back again to beans and DynaBeans.

%package -n jenkins-json-lib
Summary:        Jenkins JSON library

%description -n jenkins-json-lib
This package contains JSON library used in Jenkins.

%package help
Summary:        Help documentation for json-lib package
Provides:       %{name}-javadoc = %{version}-%{release}
Obsoletes:      %{name}-javadoc < %{version}-%{release}

%description help
Help documentation for json-lib package.

%prep
%setup -q %{name}-%{version}
tar xf %{SOURCE1}
%patch -P1 -p1

find -name "*.jar" -or -name "*.class" | xargs rm -rf

%pom_xpath_set "pom:project/pom:dependencies/pom:dependency[pom:groupId = 'org.codehaus.groovy']/pom:artifactId" groovy
%pom_remove_plugin :maven-compiler-plugin
%pom_remove_plugin :gmaven-plugin
%pom_xpath_remove "pom:project/pom:prerequisites"
%pom_xpath_remove "pom:project/pom:reporting"

rm -r src/main/jdk15/net/sf/json/JSON*.java
%pom_add_plugin org.apache.maven.plugins:maven-javadoc-plugin . '
<configuration>
  <charset>UTF-8</charset>
  <docencoding>UTF-8</docencoding>
  <sourcepath>${basedir}/src/main</sourcepath>
</configuration>'

%pom_remove_dep :commons-httpclient

install %{SOURCE2} jenkins-json-lib-%{version}/pom.xml

cd jenkins-json-lib-%{version}

%mvn_file org.kohsuke.stapler:json-lib jenkins-%{name}
%mvn_package org.kohsuke.stapler:json-lib jenkins-json-lib

cd -

%build
%mvn_file : json-lib
%mvn_build -f -- -Dproject.build.sourceEncoding=UTF-8

cd jenkins-json-lib-%{version}
%mvn_build -f
cd -

%install
%mvn_install

cd jenkins-json-lib-%{version}
%mvn_install
cd -

%files -f .mfiles
%license LICENSE.txt

%files -n jenkins-json-lib -f jenkins-json-lib-%{version}/.mfiles-jenkins-json-lib
%license LICENSE.txt

%files help -f .mfiles-javadoc
%license LICENSE.txt

%changelog
* Mon Oct 07 2024 Deyuan Fan <fandeyuan@kylinos.cn> - 2.4-23
- fix: Handle unbalanced comment string for CVE-2024-47855

* Mon Aug 22 2022 wangkai <wangkai385@h-partners.com> - 2.4-22
- Rebuild for log4j 2.17.2 fix CVE-2021-44832

* Tue May 31 2022 loong_C <loong_c@yeah.net> - 2.4-21
- update json-lib.spec

* Mon Dec 27 2021 yaoxin <yaoxin30@huawei.com> - 2.4-20
- This package depends on log4j.After the log4j vulnerability CVE-2021-45105 is fixed,the version needs to be rebuild.

* Mon Dec 20 2021 wangkai <wangkai385@huawei.com> - 2.4-19
- This package depends on log4j.After the log4j vulnerability CVE-2021-44228 is fixed,the version needs to be rebuild.

* Tue Feb 2 2021 wutao <wutao61@huawei.com> - 2.4-18
- change depdencies to groovy

* Mon Sep 14 2020 wangyue <wangyue92@huawei.com> - 2.4-17
- Fix build errors

* Sat Mar 07 2020 daiqianwen <daiqianwen@huawei.com> - 2.4-16
- Package init
